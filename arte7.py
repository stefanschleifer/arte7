#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Get direct video links from ARTE+7"""

import urllib2
import json
import sys
from bs4 import BeautifulSoup

class Arte7(object):
    """ URL as pasted by the user via brower bar """
    page_url = ''

    def __init__(self, page_url):
        self.page_url = page_url

    """
    Parse the given site to find links to JSON files
    which include links to the media files
    """
    def _parse_site(self):
        page = urllib2.urlopen(self.page_url)
        soup = BeautifulSoup(page)
        videos = soup.find_all('div', {'class': 'video-container'})
        return [v['arte_vp_url'] for v in videos]

    """ Get the JSON and find links of media files """        
    def _get_json(self):
        page = urllib2.urlopen(self._parse_site()[0])
        return json.loads(page.read())

    """ get http link of video """
    def get_http(self):
        json_result = self._get_json()
        return json_result['videoJsonPlayer']['VSR']['HTTP_REACH']['url']

    def __repr__(self):
        return u"<Arte7 %s>" % self.url

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: arte7.py <URI>"
        sys.exit(1)
    
    arte = Arte7(sys.argv[1])
    print arte.get_http()
